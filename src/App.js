import React, { Component } from "react";
import "./App.css";

import Topbar from "./components/layout/Topbar/Topbar";
import MainSlide from "./components/layout/MainSlide/MainSlide";
import CardSectiionTop from "./components/layout/CardSectionTop/CardSectionTop";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Topbar />
        <MainSlide />
        <CardSectiionTop />
      </div>
    );
  }
}

export default App;
