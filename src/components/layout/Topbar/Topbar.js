import React, { Component } from "react";
import M from "materialize-css";

import "./Topbar.css";

class Topbar extends Component {
  render() {
    document.addEventListener("DOMContentLoaded", () => {
      const elems = document.querySelectorAll(".sidenav");
      M.Sidenav.init(elems);
    });

    return (
      <div>
        <nav className="grey darken-3">
          <div className="nav-wrapper">
            <a
              href="_blank"
              data-target="mobile-demo"
              className="sidenav-trigger"
            >
              <i className="material-icons">menu</i>
            </a>
            <div className="navbar-menu-control">
              <ul className="hide-on-med-and-down">
                <li>
                  <a href="https://bitbucket.org/thelema07/">
                    <i className="material-icons">stars</i>
                  </a>
                </li>
                <li>
                  <a href="https://www.behance.net/elmezzu">
                    <i className="material-icons">work</i>
                  </a>
                </li>
                <li>
                  <a href="https://www.codewars.com/users/goldenDawn07">
                    <i className="material-icons">web</i>
                  </a>
                </li>
                <li>
                  <a href="sass.html">
                    <i className="material-icons">contacts</i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        <ul className="sidenav" id="mobile-demo">
          <li>
            <a href="sass.html">
              <i className="material-icons">stars</i>
            </a>
          </li>
          <li>
            <a href="https://www.behance.net/elmezzu">
              <i className="material-icons">work</i>
            </a>
          </li>
          <li>
            <a href="https://www.codewars.com/users/goldenDawn07">
              <i className="material-icons">web</i>
            </a>
          </li>
          <li>
            <a href="sass.html">
              <i className="material-icons">contacts</i>
            </a>
          </li>
        </ul>
      </div>
    );
  }
}

export default Topbar;
