import React, { Component } from "react";
import M from "materialize-css";

import "./MainSlide.css";

class MainSlide extends Component {
  render() {
    document.addEventListener("DOMContentLoaded", function() {
      var elems = document.querySelectorAll(".carousel");
      M.Carousel.init(elems, {
        fullWidth: true,
        indicators: true
      });
    });

    return (
      <div>
        <div className="carousel carousel-slider center">
          <div className="carousel-fixed-item center">
            <a
              href="_blank"
              className="btn waves-effect white grey-text darken-text-2"
            >
              button
            </a>
          </div>
          <div className="carousel-item grey darken-4 white-text" href="#one!">
            <h2>First Panel</h2>
            <p className="white-text">This is your first panel</p>
          </div>
          <div className="carousel-item grey darken-2 white-text" href="#two!">
            <h2>Second Panel</h2>
            <p className="white-text">This is your second panel</p>
          </div>
          <div className="carousel-item grey darken-5 white-text" href="#one!">
            <h2>First Panel</h2>
            <p className="white-text">This is your first panel</p>
          </div>
        </div>
      </div>
    );
  }
}

export default MainSlide;
