import React, { Component } from "react";

import "./CardSectionTop.css";

class CardSectionTop extends Component {
  render() {
    return (
      <div className="grey darken-3">
        <div className="row">
          <div className="card-body col s12 m4">
            <div className="card small grey darken-2">
              <div className="card-content white-text">
                <span className="card-title">Card Title</span>
                <p>
                  I am a very simple card. I am good at containing small bits of
                  information. I am convenient because I require little markup
                  to use effectively.
                </p>
              </div>
            </div>
          </div>
          <div className="card-body col s12 m4">
            <div className="card small grey darken-2">
              <div className="card-content white-text">
                <span className="card-title">Card Title</span>
                <p>
                  I am a very simple card. I am good at containing small bits of
                  information. I am convenient because I require little markup
                  to use effectively.
                </p>
              </div>
            </div>
          </div>
          <div className="card-body col s12 m4">
            <div className="card small grey darken-2">
              <div className="card-content white-text">
                <span className="card-title">Card Title</span>
                <p>
                  I am a very simple card. I am good at containing small bits of
                  information. I am convenient because I require little markup
                  to use effectively.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CardSectionTop;
